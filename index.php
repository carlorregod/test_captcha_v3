<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Captchas</title>
</head>
<body>
    <!-- Valido en main.test o en localhost -->
    <h1>Captchas!</h1>
    <form id="demo-form" method="POST" action="form-post.php">
      <label for="username">Usuario</label><input type="text" name="username" id="username"><br>
      <label for="password">Contraseña</label><input type="password" name="password" id="password"><br>

      <input type="submit" value="Enviar"> 
    </form>

    <script src="https://www.google.com/recaptcha/api.js?render=6Ldtd6IcAAAAAEYnkhk3GPLgtrD4c560qKKHjzzF"></script>


    <script>
      document.querySelector("#demo-form").onsubmit = event => {
        console.log('pasa');
        event.preventDefault();
        grecaptcha.ready(function() {

          grecaptcha.execute('6Ldtd6IcAAAAAEYnkhk3GPLgtrD4c560qKKHjzzF', {action: 'formulario'}).then( token => {
            let node_token = document.createElement('input');
            node_token.type='hidden';
            node_token.name='token';
            node_token.value=token;

            let node_action = document.createElement('input');
            node_action.type='hidden';
            node_action.name='action';
            node_action.value='formulario';

            document.querySelector("#demo-form").prepend(node_token);
            document.querySelector("#demo-form").prepend(node_action);
            //document.querySelector("#demo-form").bind('submit').submit();
            document.querySelector("#demo-form").submit();
          })

        })

      } 
    </script>
</body>
</html>